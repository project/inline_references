/**
 * @file
 *
 * Adds inline reference behavior to autocomplete nodereference elements.
 */

function update_inline (target) {
  // regex taken from nodereference.module
  var matches = target.value.match(/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/);
  if (matches && !isNaN(matches[2])) {
    var nid = matches[2];
    var inline_target = "#inline-node-" + target.id.replace('-nid-nid', '');
    $.ajax({
      dataType: 'json',
      type: 'POST',
      url: Drupal.settings.basePath + 'inline_references/' + nid,
      data: 'target=' + target.id,
      success: function(json_result){
        // Add inline info equivalent to the view page of the referenced node.
        $(inline_target).html(json_result['output']);
        var new_title = json_result['title'];
        // Reset the autocomplete value since title could potentially change.
        $("#" + target.id).val(new_title + " [nid:" + nid + "]");
        // Take away the 'Add New' link when a node is available for edit.
        $(inline_target).parent().children('.popups-reference-add-link').hide();
        // Attach behaviors in order to make the edit link into a popup.
        Drupal.attachBehaviors($(inline_target));
      },
      error: function(){
        clear_data(target, inline_target);
      }
    });
  }
  else {
    clear_data(target, inline_target);
  }
}

function clear_data(target, inline_target) {
  // Remove previously displayed inline references.
  $("#inline-node-" + target.id.replace('-nid-nid', '')).html("");
  // Make 'Add New' link visible again.
  $(inline_target).parent().children('.popups-reference-add-link').show();
}

Drupal.behaviors.inline_references = function(context) {
  // Update every inline_reference after successful popup, since a node can
  // potentially be referenced on multiple fields on the form.
  $(document).bind('popups_form_success.inline_references', function() {
    $('.inline-reference-field', context).each(function() {
      update_inline(this);
    });
  });

  $('.inline-reference-field', context).each(function() {
    // Requires http://drupal.org/node/407592 to be committed in order to work properly.
    $(this).change(function() {
      update_inline(this);
    });
    $(this).blur(function() {
      update_inline(this);
    });
  }).change();
};
