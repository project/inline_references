Module: Inline References
Author: Chris Yu
Sponsor: Classic Graphics (http://www.classicgraphics.com)

This module will append nodereference information to the edit page of a node.
With the popups api module, it will also allow a user to edit
that referenced node from the parent node's edit page.

BUGS
----
http://drupal.org/project/issues/inline_references
